/* jslint browser:true devel:true */

var i = 0, firstTime = true, list = {}, XMLURL = "http://query.yahooapis.com/v1/public/yql?q=select%20Symbol%2C%20Name%2C%20Change%2C%20ChangeinPercent%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22ING%22%2C%20%22NMR%22%2C%22SL.L%22%2C%22SLF%22%2C%22DODGX%22%2C%22SDR.L%22%2C%22AV%22%2C%22BAC%22%2C%22GLE.PA%22%2C%22NTRS%22%2C%22MTU%22%2C%22MS%22%2C%22LM%22%2C%22GS%22%2C%22KN.PA%22%2C%22BK%22%2C%22CS%22%2C%22BEN%22%2C%22DB%22%2C%22UBS%22%2C%22LGEN.L%22%2C%22JPM%22%2C%22STT%22%2C%22BCS%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=",

    /**
     * This object will contain the actual values from the yahoo response objects.
     * The Symbol and Name of the share are only given once and never changed,
     * whereas the Change will get... changed a lot
     *
     * @Symbol : The symbol of the type of shares
     * @Name   : The name of the type of shares
     * @Change : The change of a share at a point in time
     * */
    Share = function (Symbol, Name, Change) {
        "use strict";

        var symbol = Symbol,
            name = Name,
            change = Change,
            prevchange = Change;

        this.getSymbol = function () {
            return symbol;
        };

        this.getName = function () {
            return name;
        };

        this.getChange = function () {
            return change;
        };

        /**
         *  This setter first sets the prevchange;
         *
         *  @param : newChange
         */
        this.setChange = function (newChange) {
            prevchange = change;
            change = newChange;
        };

        this.getPrevChange = function () {
            return prevchange;
        };
    },

    /**
     * This object contains the data in a row in the table. The Row will take
     * its data from a Share object and store it in a cell. The values of the
     * cells gets updated by a call upon the get/set functions from the Share
     * object and will never be directly changed.
     *
     * @Symbol : The symbol of the type of shares, this variable is only used
     *           to create a corresponding Share object
     * @Name   : The name of the type of shares, this variable is only used
     *           to create a corresponding Share object
     * @Change : The change of a share at a point in time, this variable is
     *           only used to create a corresponding Share object
     * */
    Row = function (Symbol, Name, Change) {
        "use strict";

        Share.call(this, Symbol, Name, Change);

        var table = document.getElementById("stockQuotesTable"),
            row = table.insertRow(),
            symbolCell = row.insertCell(0),
            nameCell = row.insertCell(1),
            changeCell = row.insertCell(2);

        row.setAttribute("id", this.getSymbol());
        symbolCell.innerHTML = this.getSymbol();
        symbolCell.setAttribute("class", "Symbol");
        nameCell.innerHTML = this.getName();
        nameCell.setAttribute("class", "Name");
        changeCell.innerHTML = this.getChange();
        changeCell.setAttribute("class", "Change");

        this.getSymbolCell = function () {
            return symbolCell;
        };

        this.getNameCell = function () {
            return nameCell;
        };

        this.getChangeCell = function () {
            return changeCell;
        };

        /**
         * This setter also changes the color of the cell
         *
         * @newChange : The new value of the cell, which is piped to the Share
         *              objects setter and then taken with the getter to link it to the cell
         * */
        this.setChangeCell = function (newChange) {
            this.setChange(newChange);

            if (this.getChange() > 0) {
                changeCell.setAttribute("class", "positive");
            } else if (this.getChange() === 0) {
                changeCell.setAttribute("class", "neutral");
            } else if (this.getChange() < 0) {
                changeCell.setAttribute("class", "negative");
            }

            changeCell.innerHTML = this.getChange();
        };

    };

Row.prototype = new Share("", "", "");

/**
 * This function creates the header which will show the status
 * of the XMLHTTPRequest (sending request, response reader, etc)
 * */
function createHeader() {
    "use strict";

    var header = document.createElement("div");
    header.setAttribute("id", "table-status-bar");

    document.body.appendChild(header);
}

/**
 * This function searches for the header and changes its value
 *
 * @newValue : The new value of the header
 * */
function setHeader(newValue) {
    "use strict";

    var header = document.getElementById("table-status-bar");
    header.innerHTML = newValue;
}

/**
 * This function creates a table with corresponding table headers where
 * the data of the XMLHTTPRequest will be added
 * */
function createTable() {
    "use strict";

    var table = document.createElement("table"), row = table.insertRow();
    table.setAttribute("id", "stockQuotesTable");

    row.setAttribute("id", "table-header");
    row.insertCell(0).innerHTML = "Symbol";
    row.insertCell(1).innerHTML = "Name";
    row.insertCell(2).innerHTML = "Change";

    document.body.appendChild(table);
}

/**
 * This function gets called upon only after the first time creation of the XMLHTTPRequest,
 * the function will fill the list with Row objects which will be updated with new data
 * from later responses from the yahoo server
 *
 * @response : The response from the yahoo server (responseText)
 * */
function startUp(response) {
    "use strict";

    for (i = 0; i < response.length; i += 1) {
        list[i] = new Row(response[i].Symbol, response[i].Name, response[i].Change);
    }

    firstTime = false;
}

/**
 * This function gets called upon every ReadyState change. On every Ready State
 * change it wil change the value of the header with a message matching the Ready State.
 * If the header has been updated this function will update the change field of the
 * cell in the table
 *
 * @xmlreq : The XMLHTTPRequest variable;
 * */
function handleXMLRequest(xmlreq) {
    "use strict";

    switch (xmlreq.readyState) {
    case 0:
        setHeader("Initializing...");
        break;
    case 1:
        setHeader("Loading...");
        break;
    case 2:
        setHeader("Loading...");
        break;
    case 3:
        setHeader("Loading...");
        break;
    case 4:
        setHeader("Ready");

        var target, response = JSON.parse(xmlreq.responseText).query.results.quote;

        if (xmlreq.status === 200) {

            if (firstTime === true) {
                startUp(response);
            }

            for (i = 0; i < response.length; i += 1) {
                target = list[i];
                target.setChangeCell(response[i].Change);
            }
        }

        break;
    default:
        setHeader("Error?");
        break;
    }
}

/**
 * This function creates a XMLHTTPRequest which will get constant responses
 * from the yahoo server
 * */
function createXMLHTTPRequest() {
    "use strict";

    var xmlreq = new XMLHttpRequest();

    xmlreq.onreadystatechange = function () {
        handleXMLRequest(xmlreq);
    };

    xmlreq.open("GET", XMLURL, true);

    xmlreq.send(null);
}

/**
 * This function will create the table with corresponding headers and a status
 * header before creating the XMLHTTPRequest
 * */
function init() {
    "use strict";

    createTable();
    createHeader();

    setInterval(function () {
        createXMLHTTPRequest();
    }, 5000);
}

window.onload = function () {
    "use strict";

    setTimeout(init(), 1000);
};