//imports
console.log('\n======startup======\nimporting things...')
var fs = require('fs'),
	path = require('path'),
	//routes = require('./routes/routes.js'),
	express = require('express');

//setting up server
console.log('setting stuff up...');
var app = express();
	config = JSON.parse(fs.readFileSync('config.json')),
	host = config.host,
	port = config.port;

//path rerouting
console.log('rerouting things...\n========end========\n');
app.use(app.router);
app.use(express.static(path.join(__dirname, 'scripts')));
app.use(express.static(__dirname + '/routes'));

app.get('/users', function(req, res) {
	console.log('Requested: /users');
	res.send(200, 'hoi');
})

app.listen(port, host, function() {
	console.log('Listening on: ' + host + ':' + port);
});