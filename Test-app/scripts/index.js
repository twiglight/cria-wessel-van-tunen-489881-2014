var url = 'http://127.0.0.1:8008/users';

function handleXMLRequest(xhr) {
	'use strict';

	switch(xhr.readyState) {
	case 0:
		console.log('Initializing request...');
		break;
	case 1:
		console.log('Connection established...');
		break;
	case 2:
		console.log('');
		break;
	case 3:

		break;
	case 4:

		break;
	default:

		break;
	}
}

function createXMLRequest() {
	'use strict';

	var xhr = new XMLHTTPRequest();

	xhr.onreadystatechange = handleXMLRequest(xhr);
	xhr.open('GET', url, true);
	xhr.send(null);
}

function init() {
	'use strict';

	setInterval(function() {
		createXMLRequest();
	}, 1000);
}

window.onload = init();