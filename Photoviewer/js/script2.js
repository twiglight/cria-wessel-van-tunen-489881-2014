/**
 * Created by Wessel on 23-2-14.
 */

var photoviewer = {}, Events = {};

(function () {
    "use strict";

    /*
    *   @i wordt gebruikt als algemene for-loop variable
    *
    *   @album wordt gebruikt om alle images te storen. Hierdoor hoeft er geen
    *          max length aangegeven te worden voor de lijst en kan gewoon de
    *          album.length aangeroepen worden voor het aantal images
    * */
    var i, album = [{src: "1.jpg", title: "boom", landscape: false},
            {src: "2.jpg", title: "fiets", landscape: false},
            {src: "3.jpg", title: "zand", landscape: false},
            {src: "4.jpg", title: "asian", landscape: true},
            {src: "5.jpg", title: "woody", landscape: true},
            {src: "6.jpg", title: "family", landscape: true},
            {src: "7.jpg", title: "ouwe", landscape: true},
            {src: "8.jpg", title: "kind", landscape: false},
            {src: "9.jpg", title: "mafkezen", landscape: false},
            {src: "10.jpg", title: "brug", landscape: false},
            {src: "11.jpg", title: "walvis", landscape: true},
            {src: "12.jpg", title: "pier", landscape: true},
            {src: "13.jpg", title: "helemaal loco", landscape: false},
            {src: "14.jpg", title: "boswandeling", landscape: false},
            {src: "15.jpg", title: "die ene tv show", landscape: true},
            {src: "16.jpg", title: "rapists", landscape: true},
            {src: "17.jpg", title: "filter", landscape: true},
            {src: "18.jpg", title: "the sky is the limit", landscape: true},
            {src: "19.jpg", title: "water", landscape: true},
            {src: "20.jpg", title: "church thing", landscape: true},
            {src: "21.jpg", title: "vlieg vlieg", landscape: true},
            {src: "22.jpg", title: "castle", landscape: true},
            {src: "23.jpg", title: "red as my blood", landscape: true},
            {src: "24.jpg", title: "wat moet dit nu weer voorstellen", landscape: true},
            {src: "25.jpg", title: "treinen?", landscape: true}],

        Events = {

            /*
            *   Deze functie heeft dezelfde werking als onLeftClick maar vergroot de
            *   selectedImage image
            * */
            onSpacebarKey: function () {
                if (!photoviewer.zoomedImage) {
                    photoviewer.zoomedImage = true;
                    photoviewer.createEnlargedImage(photoviewer.selectedImage);
                } else {
                    photoviewer.zoomedImage = false;
                    photoviewer.collapseEnlargedImage();
                }
            },
            /*
            *   Deze functie zet de eerst volgende item in de pictureList als selectedImage,
            *   als het eind van de pictureList bereikt is wordt deze op het eerste item van
            *   de pictureList als selectedImage gezet
            * */
            onLeftArrowKey: function () {
                if ((Number(photoviewer.selectedImage.alt) - 1) !== -1) {
                    photoviewer.selectedImage = photoviewer.pictureList[Number(photoviewer.selectedImage.alt) - 1];
                } else {
                    photoviewer.selectedImage = photoviewer.pictureList[photoviewer.pictureList.length - 1];
                }
                if (photoviewer.zoomedImage) {
                    photoviewer.createEnlargedImage(photoviewer.selectedImage);
                }
            },
            /*
            *   Deze functie zet de image onder de selectedImage als selectedImage, deze wordt berekend
            *   door de getRasterSize bij de pictureList te tellen. Als het pictureList item niet bestaat
            *   wordt de bovenste image in de colom als selectedImage gezet
            * */
            onUpArrowKey: function () {
                if ((Number(photoviewer.selectedImage.alt) - photoviewer.getRasterSize()) >= 0) {
                    photoviewer.selectedImage = photoviewer.pictureList[Number(photoviewer.selectedImage.alt) - photoviewer.getRasterSize()];
                } else {
                    photoviewer.selectedImage = photoviewer.pictureList[photoviewer.pictureList.length + (Number(photoviewer.selectedImage.alt) - photoviewer.getRasterSize())];
                }
                if (photoviewer.zoomedImage) {
                    photoviewer.createEnlargedImage(photoviewer.selectedImage);
                }
            },
            /*
             *   Deze functie zet het vorige item in de pictureList als selectedImage,
             *   als het begin van de pictureList bereikt is wordt deze op het laatste item van
             *   de pictureList als selectedImage gezet
             * */
            onRightArrowKey: function () {
                if ((Number(photoviewer.selectedImage.alt) + 1) !== photoviewer.pictureList.length) {
                    photoviewer.selectedImage = photoviewer.pictureList[Number(photoviewer.selectedImage.alt) + 1];
                } else {
                    photoviewer.selectedImage = photoviewer.pictureList[0];
                }
                if (photoviewer.zoomedImage) {
                    photoviewer.createEnlargedImage(photoviewer.selectedImage);
                }
            },
            /*
             *   Deze functie zet de image boven de selectedImage als selectedImage, deze wordt berekend
             *   door de getRasterSize bij de pictureList te tellen. Als het pictureList item niet bestaat
             *   wordt de onderste image in de colom als selectedImage gezet
             * */
            onDownArrowKey: function () {
                if ((Number(photoviewer.selectedImage.alt) + photoviewer.getRasterSize()) < photoviewer.pictureList.length) {
                    photoviewer.selectedImage = photoviewer.pictureList[Number(photoviewer.selectedImage.alt) + photoviewer.getRasterSize()];
                } else {
                    photoviewer.selectedImage = photoviewer.pictureList[Number(photoviewer.selectedImage.alt) + photoviewer.getRasterSize() - photoviewer.pictureList.length];
                }
                if (photoviewer.zoomedImage) {
                    photoviewer.createEnlargedImage(photoviewer.selectedImage);
                }
            },

            /*
            *   Een Event Handler die, mits de juiste key wordt in gedrukt, de juiste functie aanroept
            *
            *   32: Spacebar
            *   37: Left  arrow
            *   38: Up    arrow
            *   39: Right arrow
            *   40: Down  arrow
            *
            *   Voor andere keys wordt niks gedaan
            *
            *   @event hier wordt de keyCode van de aangeslagen key uit gehaald
            * */
            onKeyEvent: function (event) {
                switch (event.keyCode) {
                case 32:
                    Events.onSpacebarKey();
                    break;
                case 37:
                    Events.onLeftArrowKey();
                    break;
                case 38:
                    Events.onUpArrowKey();
                    break;
                case 39:
                    Events.onRightArrowKey();
                    break;
                case 40:
                    Events.onDownArrowKey();
                    break;
                default:
                    break;
                }
            },
            /*
            *   Deze functie maak een een largeImage aan met de event target
            *   of verwijderd de largeImage als dezeal bestaat.
            *
            *   @event hieruit wordt de aangeklikte image gehaald
            * */
            onLeftClick: function (event) {
                if (photoviewer.pictureList.length > 1) {
                    photoviewer.selectedImage = event.target;
                    if (event.target.getAttribute("id") !== "largeImage") {
                        photoviewer.createEnlargedImage(photoviewer.selectedImage);
                    } else if (photoviewer.zoomedImage) {
                        photoviewer.collapseEnlargedImage();
                    }
                }

            },
            /*
            *   Deze functie verwijderd een image en haalt deze uit de album lijst.
            *   Hierna wordt de loadImages aangeroepen waardoe de pictureList wordt
            *   gereset met de actueele album items (met dus 1 image minder dan voorheen)
            *
            *   @event hieruit wordt de aangeklikte image gehaald
            * */
            onRightClick: function (event) {
                event.preventDefault();
                album.splice(Number(event.target.alt), 1);
                document.body.innerHTML = null;
                photoviewer.loadImages();
            }

        },

        Photoviewer = {

            /*
            *   @selectedImage Hierin wordt de laatst geselecteerde image in gestored waar de keyEvents mee
            *                  werken aangezien hier geen event mee kan worden meegegeven
            *   @zoomedImage Een boolean waarmee wordt aangegeven of er op een de selectedImage is ingezoomed
            * */
            selectedImage: null,
            zoomedImage: false,

            /*
            *   Deze functie vult de pictureList met de images die in de album lijst beschreven staan.
            *   Bij elke pictureList image worden ook twee Event Listeners toegevoegd. De images uit de
            *   pictureList worden in de window geplaats nadat deze in de containers geplaatst zijn.
            *   Verder zet de functie het eerste item van pictureList als selectedImage
            *
            *   @rightClick verwijst naar de event methode om zo een jsLint error te omzeilen
            *   @leftClick verwijst naar de event methode om zo een jsLint error te omzeilen
            * */
            loadImages: function () {
                var imgcontainer,
                    rightClick = function (event) { Events.onRightClick(event); },
                    leftClick = function (event) { Events.onLeftClick(event); };

                photoviewer.pictureList = [];

                for (i = 0; i < album.length; i += 1) {
                    imgcontainer = document.createElement("div");
                    imgcontainer.setAttribute("class", "imgcontainer");

                    photoviewer.pictureList[i] = document.createElement("img");
                    photoviewer.pictureList[i].setAttribute("src", "img/" + album[i].src);
                    photoviewer.pictureList[i].setAttribute("alt", i);
                    photoviewer.pictureList[i].setAttribute("title", album[i].title);
                    photoviewer.pictureList[i].landscape = album[i].landscape;

                    photoviewer.pictureList[i].addEventListener("contextmenu", rightClick, false);
                    photoviewer.pictureList[i].addEventListener("click", leftClick, false);

                    imgcontainer.appendChild(photoviewer.pictureList[i]);

                    document.body.appendChild(imgcontainer);

                }
                photoviewer.selectedImage = photoviewer.pictureList[0];

                photoviewer.resizeEvent();
            },

            /*
            *   @img de image die vergoot moet worden weergegeven
            *
            *   Deze functie maakt een popup van de meegegeven img waarop deze vergroot word weergegeven
            *
            *   @largeImage deze variable wordt gevuld met een nieuwe of reeds bestaande largeImage
            *   @text deze variable wordt gevuld met een nieuwe of reeds bestaande largeImage
            * */
            createEnlargedImage: function (img) {
                var largeImage = null, text = null;

                if (document.getElementById("largeImage") === null) {
                    largeImage = document.createElement("img");
                    largeImage.setAttribute("id", "largeImage");
                    largeImage.setAttribute("alt", "largeImage");
                    text = document.createElement("p");
                    text.setAttribute("id", "text");

                } else {
                    largeImage = document.getElementById("largeImage");
                    largeImage.style.display = "block";
                    text = document.getElementById("text");
                    text.removeChild(text.firstChild);
                    text.style.display = "block";
                }

                largeImage.setAttribute("src", img.src);
                text.appendChild(document.createTextNode(img.title));

                largeImage.style.position = "absolute";
                text.style.position = "absolute";

                largeImage.style.top = (window.innerHeight / 2) - (img.naturalHeight / 2) + "px";
                largeImage.style.left = (window.innerWidth / 2) - (img.naturalWidth / 2) + "px";

                text.style.width = 200 + "px";

                text.style.top = (largeImage.offsetTop / 2) + "px";
                text.style.left = ((window.innerWidth / 2) - (200 / 2)) + "px";

                largeImage.addEventListener("click", function (event) {
                    Events.onLeftClick(event);
                }, false);

                document.body.appendChild(largeImage);
                document.body.appendChild(text);

                photoviewer.zoomedImage = true;
            },

            /*
            *   Deze functie verwijderd de vergrote image en de bijbehoordende text
            * */
            collapseEnlargedImage: function () {
                document.getElementById("largeImage").style.display = "none";
                document.getElementById("text").style.display = "none";
            },

            /*
            *   Deze functie geeft de naar boven afgeronde wortel van de lengte van de pictureList terug.
            *   Dit wordt gebruikt om het raster mee te berekenen
            * */
            getRasterSize: function () {
                return Math.ceil(Math.sqrt(photoviewer.pictureList.length));
            },

            /*
            *   Deze functie wordt telkens aangeroepen als de window geresized wordt. Eerst zoekt deze functie alle bestaande items op het scherm
            *   en vervolgens wordt de nieuwe breedte, hoogte, top en left berekend
            * */
            resizeEvent: function () {
                var elements = document.getElementsByTagName("div"), largeImage = document.getElementById("largeImage"), text = document.getElementById("text");

                for (i = 0; i < elements.length; i += 1) {
                    elements[i].style.width = ((window.innerWidth / photoviewer.getRasterSize()) - 8) + "px";
                    elements[i].style.height = ((window.innerHeight / photoviewer.getRasterSize()) - 8) + "px";

                    if (elements[i].firstChild.landscape) {
                        elements[i].firstChild.style.width = "70%";
                    } else {
                        elements[i].firstChild.style.height = "100%";
                    }

                }

                if (largeImage !== null) {
                    largeImage.style.top = (window.innerHeight / 2) - (largeImage.naturalHeight / 2) + "px";
                    largeImage.style.left = ((window.innerWidth / 2) - (largeImage.naturalWidth / 2)) + "px";

                    text.style.top = (largeImage.offsetTop / 2) + "px";
                    text.style.left = ((window.innerWidth / 2) - (200 / 2)) + "px";
                }

            }

        };

    window.photoviewer = Photoviewer;
    window.onresize = photoviewer.resizeEvent;
    window.addEventListener("keydown", function (event) { Events.onKeyEvent(event); }, false);
}());

window.onload = function () {
    "use strict";

    window.photoviewer.loadImages();
};